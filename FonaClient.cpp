#include "FonaClient.h"



void FonaClient::powerOn()
{
	//power off or power on if was off 
	digitalWrite(FONA_PWRKEY, LOW);
	delay(1000); // For SIM5320
	digitalWrite(FONA_PWRKEY, HIGH);
	delay(3000);
	//power on / dont power off if on
	digitalWrite(FONA_PWRKEY, LOW);
	delay(100); // For SIM5320
	digitalWrite(FONA_PWRKEY, HIGH);
	//module should be ready here
}

void FonaClient::moduleSetup()
{
	// The baud rate always resets back to default (115200) after being powered
	// powered down or shutting off, so let's try 115200 first. Hats off to
	// anyone who can figure out how to make it remember the new baud rate even
	// after being power cycled!

	if (!fona.begin(*fonaSerial)) {
		Serial.println(F("Couldn't find FONA at 115200 baud"));
	}

	type = fona.type();
	Serial.println(F("FONA is OK"));
	Serial.print(F("Found "));
	switch (type) {
	case SIM800L:
		Serial.println(F("SIM800L")); break;
	case SIM800H:
		Serial.println(F("SIM800H")); break;
	case SIM808_V1:
		Serial.println(F("SIM808 (v1)")); break;
	case SIM808_V2:
		Serial.println(F("SIM808 (v2)")); break;
	case SIM5320A:
		Serial.println(F("SIM5320A (American)")); break;
	case SIM5320E:
		Serial.println(F("SIM5320E (European)")); break;
	case SIM7000A:
		Serial.println(F("SIM7000A (American)")); break;
	case SIM7000C:
		Serial.println(F("SIM7000C (Chinese)")); break;
	case SIM7000E:
		Serial.println(F("SIM7000E (European)")); break;
	case SIM7000G:
		Serial.println(F("SIM7000G (Global)")); break;
	case SIM7500A:
		Serial.println(F("SIM7500A (American)")); break;
	case SIM7500E:
		Serial.println(F("SIM7500E (European)")); break;
	default:
		Serial.println(F("???")); break;
	}


	// Print module IMEI number.
	uint8_t imeiLen = fona.getIMEI(imei);
	if (imeiLen > 0) {
		Serial.print("Module IMEI: "); Serial.println(imei);
	}
}

bool FonaClient::netStatus()
{
	int n = fona.getNetworkStatus();

	Serial.print(F("Network status ")); Serial.print(n); Serial.print(F(": "));
	if (n == 0) Serial.println(F("Not registered"));
	if (n == 1) Serial.println(F("Registered (home)"));
	if (n == 2) Serial.println(F("Not registered (searching)"));
	if (n == 3) Serial.println(F("Denied"));
	if (n == 4) Serial.println(F("Unknown"));
	if (n == 5) Serial.println(F("Registered roaming"));

	if (!(n == 1 || n == 5)) return false;
	else return true;
}

FonaClient::FonaClient() : fona(FONA_RST)
{
	initialized = false;
}


FonaClient::~FonaClient()
{
}

void FonaClient::init(HardwareSerial* s)
{
	fonaSerial = s;
	fonaSerial->begin(115200);
	// Assign pins 10 & 11 SERCOM functionality
	pinPeripheral(10, PIO_SERCOM);
	pinPeripheral(11, PIO_SERCOM);

	//Hard reset module
	pinMode(FONA_RST, OUTPUT);
	digitalWrite(FONA_RST, HIGH); // Default state

	pinMode(FONA_PWRKEY, OUTPUT);
	powerOn(); // Power on the module
	moduleSetup(); // Establishes first-time serial comm and prints IMEI

	//fona.unlockSIM("pin");

	fona.setNetworkSettings(F("gprs.swisscom.ch")); //Swisscom

	if (!fona.enableGPRS(false)) Serial.println(F("Failed to disable GPRS!"));
	while (!fona.enableGPRS(true)) {
		Serial.println(F("Failed to enable GPRS, retrying..."));
		delay(2000); // Retry every 2s
	}
	Serial.println(F("Enabled GPRS!"));
	initialized = true;
}

int FonaClient::connect(IPAddress ip, uint16_t port)
{
	char ipA[17];
	sprintf(ipA, "%03d.%03d.%03d.%03d", ip[0], ip[1], ip[2], ip[3]);
	return fona.TCPconnect(ipA, port);
}

int FonaClient::connect(const char * host, uint16_t port)
{
	char t[strlen(host)+1];
	sprintf(t, "%s", host);
	return fona.TCPconnect(t, port);
}

size_t FonaClient::write(uint8_t a)
{
	char c = a;
	if (fona.TCPsend(&c, 1)) return 1;
	else return 0;
}

size_t FonaClient::write(const uint8_t * buf, size_t size)
{
	char* c = (char*)buf;
	size_t totalSize = 0;
	for (int i = 0; i < (uint8_t)size / 1024; i++)
	{
		if (fona.TCPsend(c, 1024)) totalSize += 1024;
		else break;
	}
	if (fona.TCPsend(c, size-totalSize)) return size - totalSize;
	else return 0;
}

int FonaClient::available()
{
	return fona.TCPavailable();
}

int FonaClient::read()
{
	uint8_t r;
	if(fona.TCPread(&r, 1) == 1) return r;
	else return -1;
}

int FonaClient::read(uint8_t * buf, size_t size)
{
	return fona.TCPread(buf, size);
}

int FonaClient::peek()
{
	return 0;
}

void FonaClient::flush()
{
}

void FonaClient::stop()
{
	fona.TCPclose();
}

uint8_t FonaClient::connected()
{
	return fona.TCPconnected();
}

FonaClient::operator bool()
{
	return initialized;
}
