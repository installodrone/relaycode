#include <RH_RF95.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include "FonaClient.h"


//Create a new serial for communicating with the GSM module


#define SIMCOM_3G // SIM5320

#define RFM95_CS      8
#define RFM95_IRQ     3
#define RFM95_RST     4

#define LED 13
// For CloudMQTT find these under the "Details" tab:
#define MQTT_SERVER      "34.251.42.52"
#define MQTT_PORT		 17483
#define MQTT_PASSWORD    "relay"
#define MQTT_USERNAME    "relay"
#define MQTT_KEY         "879a75f2-1387-49c7-ac81-432f6ea59c76"

Uart Serial2(&sercom1, 11, 10, SERCOM_RX_PAD_0, UART_TX_PAD_2);
HardwareSerial *fonaSerial = &Serial2;

void SERCOM1_Handler()
{
	Serial2.IrqHandler();
}
// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_IRQ);
FonaClient fona;
PubSubClient client(MQTT_SERVER, MQTT_PORT, fona);

uint16_t battLevel = 0; // Battery level (percentage)
uint8_t counter = 0;

uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
uint8_t len = sizeof(buf);
void setup() {
	Serial.begin(115200);

	pinMode(LED, OUTPUT);
	pinMode(RFM95_RST, OUTPUT);
	digitalWrite(RFM95_RST, HIGH);

	Serial.println("Feather LoRa RX Test!");

	// manual reset
	digitalWrite(RFM95_RST, LOW);
	delay(10);
	digitalWrite(RFM95_RST, HIGH);
	delay(10);

	while (!rf95.init()) {
		Serial.println("LoRa radio init failed");
		while (1);
	}
	Serial.println("LoRa radio init OK!");

	if (!rf95.setFrequency(433.0)) {
		Serial.println("setFrequency failed");
		while (1);
	}
	Serial.print("Set Freq to: "); Serial.println(433.0);

	// The default transmitter power is 13dBm, using PA_BOOST.
	// If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then 
	// you can set transmitter powers from 5 to 23 dBm:
	rf95.setTxPower(13, false);

	fona.init(&Serial2);

}
void callback(char* t, uint8_t* a, unsigned int b) {
	
}
void loop() 
{
	if (rf95.available())
	{
		// Should be a message for us now
		uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
		uint8_t len = sizeof(buf);

		if (rf95.recv(buf, &len))
		{
			digitalWrite(LED, HIGH);
			Serial.print("Got: ");
			Serial.println((char*)buf);
			Serial.print("RSSI: ");
			Serial.println(rf95.lastRssi(), DEC);
		
			// Send a reply
			uint8_t data[] = "ACK";
			rf95.send(data, sizeof(data));
			rf95.waitPacketSent();
			Serial.println("Sent acknowledge");
			digitalWrite(LED, LOW);

			//client object makes connection to server
			if (!client.connected()) {
				Serial.println("Connecting to MQTT server");
				//Authenticating the client object
				if (client.connect("relay", MQTT_USERNAME, MQTT_PASSWORD)) {
					Serial.println("Connected to MQTT server");
					client.publish("test", (char*)buf);
				}
				else {
					Serial.println("Could not connect to MQTT server");
				}
			}
			else
			{
				client.publish("test", buf, strlen((char*)buf));
			}
			//client.disconnect();
		}
		else
		{
			Serial.println("Receive failed");
		}
	}
}
