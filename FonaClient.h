#pragma once
#define SUCCESS 1
#define TIMED_OUT - 1
#define INVALID_SERVER - 2
#define TRUNCATED - 3
#define INVALID_RESPONSE - 4

#define FONA_RX 10
#define FONA_TX 11
#define FONA_RST 6
#define FONA_PWRKEY 9

#pragma once
#include "Adafruit_FONA.h"
#include <Client.h>
#include "wiring_private.h" // pinPeripheral() function


class FonaClient : public Client
{
private :
	bool initialized;
	uint8_t type;
	char imei[16]; // Use this for device ID
	HardwareSerial* fonaSerial;
	Adafruit_FONA_3G fona;
	void powerOn();
	void moduleSetup();
	bool netStatus();
public:
	FonaClient();
	~FonaClient();
	void init(HardwareSerial* s);
	// H�rit� via Client
	virtual int connect(IPAddress ip, uint16_t port) override;
	virtual int connect(const char * host, uint16_t port) override;
	virtual size_t write(uint8_t) override;
	virtual size_t write(const uint8_t * buf, size_t size) override;
	virtual int available() override;
	virtual int read() override;
	virtual int read(uint8_t * buf, size_t size) override;
	virtual int peek() override;
	virtual void flush() override;
	virtual void stop() override;
	virtual uint8_t connected() override;
	virtual operator bool() override;
};

